/************************************************************
 * Importation des données de diverses sources dans la bdd  *
 ************************************************************/

/* Importation nettoyée des doublons des relevés (carre>date>point) de la bdd de Kevin
 * INFO : Fait
 */

drop table releves_mnhn
;

create table releves_mnhn
  as
    select distinct
      id
      , concat(sdm.carre_num :: int, sdm.date :: date, sdm.point_ok :: int) as concat
    from stoc_data_mnhn sdm
;

create index on releves_mnhn (concat)
;

truncate data.stoc_releves cascade
;

insert into data.stoc_releves (date, point_num, altitude, passage_mnhn, source_bdd, geom, site,
                               nuage, pluie, visibilite, vent,
                               p_milieu, p_type, p_cat1, p_cat2, p_ss_cat1, p_ss_cat2,
                               s_milieu, s_type, s_cat1, s_cat2, s_ss_cat1, s_ss_cat2)
  select distinct
    spg.date
    , spg.carre
    , spg.point
    , spg.altitude
    , spg.passage
    , 'FEPS RA'         source
    , spg.geom
    , case when spg.carre > 980000
    then true
      else false end as site
    , sdp.nuage
    , sdp.pluie
    , sdp.visibilite
    , sdp.vent
    , sdp.p_milieu
    , sdp.p_type
    , sdp.p_cat1
    , sdp.p_cat2
    , sdp.p_ss_cat_1
    , sdp.p_ss_cat_2
    , sdp.s_milieu
    , sdp.s_type
    , sdp.s_cat1
    , sdp.s_cat2
    , sdp.s_ss_cat_1
    , sdp.s_ss_cat_2
  from lpo07_fcl.stoc_point_geom spg left join lpo07_fcl.stoc_data_point_km sdp
      on (spg.carre, spg.date, spg.point) = (sdp.carre_num, sdp.date, sdp.point)
;


select count(*)
from data.stoc_releves
;

/* Importation nettoyée des doublons des observations (releve>distance>espece) de la bdd de kevin
 * INFO: Fait
 */

create index on lpo07_fcl.stoc_inv (carre, date, point)
;

drop table stoc_inv_w_uniq_data
;

create table stoc_inv_w_uniq_data as (
  select distinct *
  from stoc_inv)
;


insert into data.stoc_observations (
  carre_numnat,
  date,
  time,
  id_releve,
  point,
  codesp_euring,
  nombre,
  source,
  passage,
  distance_v2
)
  with rel as (
      select
        id
        , concat(carre_numnat, date, point_num) as concat
      from data.stoc_releves ),
      datas as (
        select
          distinct
          obs.carre
          , obs.date
          , obs.heure
          , obs.point
          , obs.code_esp                           as codesp_euring
          , sum(obs.nombre)                        as nombre
          , 'FEPS RA'                              as source
          , obs.passage
          , dcd."libellé"                          as libelle
          , concat(obs.carre, obs.date, obs.point) as concat
        from lpo07_fcl.stoc_inv_w_uniq_data as obs left join data.stoc_dico_code_distances dcd
            on obs.distance = dcd.code
        group by
          obs.carre
          , obs.date
          , obs.heure
          , obs.point
          , obs.code_esp
          , obs.passage
          , dcd."libellé"
          , (obs.carre, obs.date, obs.point)
    )
  select
    distinct
    obs.carre
    , obs.date
    , obs.heure
    , rel.id    as id_releve
    , obs.point
    , obs.codesp_euring
    , obs.nombre
    , 'FEPS RA' as source
    , obs.passage
    , obs.libelle
  from datas as obs
    , rel
  where
    obs.concat = rel.concat
;

/* info: Correction de l'erreur de maille pour les données attribuées au carré 380904 */
-- update stoc_inv_w_uniq_data as datas
-- set
--   carre = carre_num
-- from stoc_data_mnhn as dm
-- where
--   carre = 380904
--   and carre_num in (380904, 740904)
--   and (datas.date :: date, datas.heure :: time, datas.point :: int) =
--       (dm.date :: date, dm.heure :: time, split_part(export_stoc_text_eps, '°', 2) :: int)
-- ;

/* importation des données auvergnates
    1 - génération des relevés
    2 - génération des observations
*/

/* relevés auvergnats
 * INFO: Fait
 */

insert into data.stoc_releves (
  date, heure, observateur, carre_numnat, point_num, altitude,
  nuage, pluie, vent, visibilite,
  p_milieu, p_type, p_cat1, p_cat2, p_ss_cat1, p_ss_cat2,
  s_milieu, s_type, s_cat1, s_cat2, s_ss_cat1, s_ss_cat2,
  site, source_bdd, geom, passage_mnhn
)
  select distinct
    date :: date
    , heure :: time
    , observateur
    , site                                 as carre_numnat
    , (split_part(n_point, '°', 2)) :: int as point_num
    , altitude
    , nuage
    , pluie
    , vent
    , visi
    , p_milieu
    , p_type
    , p_cat1
    , p_cat2
    , e_ss_cat1
    , e_ss_cat2
    , s_milieu
    , s_type
    , s_cat1
    , s_cat2
    , s_ss_cat1
    , s_ss_cat2
    , false                                as site
    , 'FEPS AUV'                           as source_bdd
    , st_transform(
          st_setsrid(
              st_makepoint(longitude, latitude),
              27582),
          2154)                            as geom
    , n_passage                            as passage
  from lpo07_fcl.stoc_data_auvergne_feps_2014
  where etude like 'STOC-EPS'
        and n_point ilike 'Point%'
;

cluster data.stoc_releves
using stoc_releves_carre_numnat_date_point_num_idx
;

/* Données auvergnates
 * INFO: Fait
 */

insert into data.stoc_observations (
  carre_numnat,
  date,
  time,
  id_releve,
  point,
  codesp_euring,
  nombre,
  source,
  passage,
  distance_v2
)
  with rel as (
      select
        id
        , concat(carre_numnat, date, point_num) as concat
      from data.stoc_releves ),
      datas as (
        select
          distinct
            obs.site                                                                 as carre
          , obs.date :: date
          , obs.heure :: time
          , (split_part(n_point, '°', 2)) :: int                                     as point
          , obs.code_sp                                                              as codesp_euring
          , sum(obs.nombre)                                                          as nombre
          , 'FEPS RA'                                                                as source
          , obs.n_passage
          , dcd."libellé"                                                            as distance
          , concat(obs.site, obs.date :: date, (split_part(n_point, '°', 2)) :: int) as concat
        from lpo07_fcl.stoc_data_auvergne_feps_2014 as obs left join data.stoc_dico_code_distances dcd
            on obs.dist_contact = dcd.code
        where etude like 'STOC-EPS'
              and n_point ilike 'Point%'
        group by
          obs.site
          , obs.date
          , obs.heure
          , (split_part(n_point, '°', 2))
          , obs.code_sp
          , obs.n_passage
          , dcd."libellé"
          , concat(obs.site, obs.date :: date, (split_part(n_point, '°', 2)) :: int)
    )
  select
    distinct
    obs.carre
    , obs.date
    , obs.heure
    , rel.id     as id_releve
    , obs.point
    , obs.codesp_euring
    , obs.nombre
    , 'FEPS AUV' as source
    , obs.n_passage
    , obs.distance
  from datas as obs left join rel
      on obs.concat = rel.concat
;


-- select st_astext(st_centroid(st_transform(geom, 27582)))
-- from administratif.territoire_commune
-- where nom ilike 'trévol'
-- ;
--
-- select (split_part(n_point, '°',2)), site, count(*) from lpo07_fcl.stoc_data_auvergne_feps_2014 where (split_part(n_point, '°',2)) like '' group by (split_part(n_point, '°',2)), site and ;
--
-- select * from lpo07_fcl.stoc_data_auvergne_feps_2014  where (split_part(n_point, '°',2)) = '';
-- select etude, saisie_mamm, sp, count(*) from lpo07_fcl.stoc_data_auvergne_feps_2014 group by etude, saisie_mamm, sp;
--

/* importation des données MNHN et remplacement
    1 - génération des relevés
    2 - génération des observations
*/

/* Vérification des doublons avec les données FEPS Auvergnates et RA */
drop table if exists rels
;

create temporary table rels as (
  select concat(carre_numnat, date, point_num) as concat
  from data.stoc_releves rel
  where source_bdd not like 'MNHN')
;

create index on rels (concat)
;


explain with ids as (select id
                     from releves_mnhn, rels
                     where releves_mnhn.concat = rels.concat)
select
  count(distinct concat)
  , count(distinct id)
from releves_mnhn
where id not in (
  select id
  from ids)
;

explain select count(releves_mnhn.*)
        from releves_mnhn, rels
        where releves_mnhn.concat != rels.concat
;

select *
from data.stoc_releves dr
where (dr.carre_numnat, dr.point_num, dr.date) = (421050, 3, '2011-05-22')
;


select *
from data.stoc_observations
where id_releve in (
  select distinct dr.id
  from data.stoc_releves dr left join data.stoc_observations so
      on dr.id = so.id_releve
    , lpo07_fcl.stoc_data_mnhn sdm
  where (dr.carre_numnat, dr.point_num, dr.date) = (sdm.carre_num, sdm.point_ok, sdm.date) and so.nombre is null
  group by dr.id, (dr.carre_numnat, dr.point_num, dr.date), (sdm.carre_num, sdm.point_ok, sdm.date))
;

select *
from data.stoc_observations
where id_releve in (
  select distinct dr.id
  from data.stoc_releves dr
  where dr.carre_numnat in (
    select distinct carre_num
    from lpo07_fcl.stoc_data_mnhn sdm
  ))
;

/* Insertion des relevés MNHN non présents dans les données FEPS importées */
/* info: fait 13577 relevés */

delete from data.stoc_releves
where source_bdd like 'MNHN'
;

insert into data.stoc_releves (
  date, heure, observateur, carre_numnat, point_num, altitude,
  nuage, pluie, vent, visibilite,
  p_type, p_cat1, p_cat2,
  s_milieu, s_type, s_cat1, s_cat2,
  site, source_bdd, geom, passage_mnhn)
  with ids as (
    /* Sélection des relevés en commun avec les données FEPS (pour exclusion) */
      select id
      from releves_mnhn, rels
      where releves_mnhn.concat = rels.concat
  ),
      datas_to_keep as (
      /* Sélection des relevés pas dans les données FEPS Auv et RA (not in ids) */
        select distinct id
        from releves_mnhn
        where id not in (
          select id
          from ids)
    )
  /* Génération des données d'après les id de datas_to_keep */
  select distinct
    date :: date
    , heure :: time
    , observateur
    , carre_num
    , (split_part(export_stoc_text_eps, '°', 2)) :: int as point
    , altitude
    , nullif(nuage, 'NA') :: int                           nuage
    , nullif(pluie, 'NA') :: int                           pluie
    , nullif(vent, 'NA') :: int                            vent
    , nullif(visibilite, 'NA') :: int                      visibilite
    , nullif(p_type, 'NA') :: int                          p_type
    , nullif(p_cat1, 'NA') :: int                          p_cat1
    , nullif(p_cat2, 'NA') :: int                          p_cat2
    , nullif(s_milieu, 'NA')                               s_milieu
    , nullif(s_type, 'NA') :: int                          s_type
    , nullif(s_cat1, 'NA') :: int                          s_cat1
    , nullif(s_cat2, 'NA') :: int                          s_cat2
    , case when etude like 'STOC_ONF'
    then true
      else false end                                    as site
    , 'MNHN'                                            as source
    , geom
    , num_passage
  from stoc_data_mnhn
  where id in (
    select id
    from datas_to_keep)
;

insert into data.stoc_observations (
  carre_numnat,
  date,
  time,
  id_releve,
  point,
  codesp_euring,
  nombre,
  source,
  passage,
  distance_v2
)
  with rel as (
    /* génération d'id unique de relevé pour association des données mnhn (concatenation du carre, date, point) à la table stoc_releve */
      select
        id
        , concat(carre_numnat :: int, date :: date, point_num :: int) as concat
      from data.stoc_releves
      where source_bdd like 'MNHN' ),
      datas as (
      /* Table des obs avec la génération d'une concaténation comparable avec la précédente */
        select
          distinct
            obs.carre_num :: int                                     as carre
          , obs.date :: date
          , nullif(obs.heure, 'NA') :: time                          as heure
          , point_ok :: int                                          as point
          , obs.espece                                               as codesp_euring
          , sum(obs.nombre)                                          as nombre
          , obs.passage_v2
          , obs.distance_v2                                          as distance
          , concat(obs.carre_num, obs.date :: date, point_ok :: int) as concat
        from lpo07_fcl.stoc_data_mnhn as obs
        group by
          obs.carre_num
          , obs.date
          , obs.heure
          , obs.point_ok
          , obs.espece
          , obs.passage_v2
          , obs.distance_v2
          , concat(obs.carre_num, obs.date :: date, point_ok :: int)
    )
  /* Récuparétion des id de relevés d'après l'association des deux précédentes requêtes*/
  select
    distinct
    obs.carre
    , obs.date
    , obs.heure
    , rel.id as id_releve
    , obs.point
    , obs.codesp_euring
    , obs.nombre
    , 'MNHN' as source
    , obs.passage_v2
    , obs.distance
  from datas as obs, rel
  where obs.concat = rel.concat
;

create index on data.stoc_dico_code_points (libelle)
;

create index on data.stoc_dico_code_points (code)
;

create index on data.stoc_dico_code_points (libelle_vn)
;

create index on data.stoc_dico_code_points (type_code)
;

/* Import des données VisioNature */
do $$
declare

    t1 cursor is select distinct (split_part(split_part("PLACE",' ',1), '_', 1)::int, "DATE" :: date, "POINT_NUMBER") as data
                 from lpo07_fcl.stoc_vn_2018;

  total    integer := (
    select count(distinct (split_part(split_part("PLACE",' ',1), '_', 1)::int, "DATE" :: date, "POINT_NUMBER"))
    from lpo07_fcl.stoc_vn_2018);

  iterator float4 := 1;

  percent  float;

begin


  for datas in t1
  loop
    iterator := iterator + 1
  ;


    raise notice 'iterator %', iterator
  ;

    percent := round(cast(((iterator / total) * 100) as numeric), 4)
  ;

    raise notice 'progression: %', percent
  ;

    raise notice 'data >>> %', datas.data
  ;


    delete from data.stoc_releves
    where (carre_numnat, date :: date, point_num) = datas.data
  ;

    insert into data.stoc_releves (
      date, heure, observateur, carre_numnat, point_num, altitude,
      nuage, pluie, vent, visibilite,
      p_milieu, p_type, p_cat1, p_cat2, p_ss_cat1, p_ss_cat2,
      s_milieu, s_type, s_cat1, s_cat2, s_ss_cat1, s_ss_cat2,
      site, source_bdd, geom, passage_mnhn)
    /* Génération des données d'après les id de datas_to_keep */
      select distinct
        "DATE" :: date
        , "TIME_START" :: time
        , "NAME"
        , split_part(split_part("PLACE",' ',1), '_', 1)::int                                                              as place
        , "POINT_NUMBER"                                                                            as point
        , round(avg(distinct "ALTITUDE"))                                                                    as altitude
        , c_nuage.code :: int                                                                       as code_nuage
        , c_pluie.code :: int                                                                       as code_pluie
        , c_vent.code :: int                                                                        as code_wind
        , c_visi.code :: int                                                                        as code_visi
        , c_pmil1.code                                                                              as p_milieu
        , c_ptyp1.code                                                                              as p_type
        , c_pcat1.code                                                                              as p_cat1
        , c_pcat2.code                                                                              as p_cat2
        , c_psscat1.code                                                                            as p_sscat1
        , c_psscat2.code                                                                            as p_sscat2
        , c_smil1.code                                                                              as s_milieu
        , c_styp1.code                                                                              as s_type
        , c_scat1.code                                                                              as s_cat1
        , c_scat2.code                                                                              as s_cat2
        , c_ssscat1.code                                                                            as s_sscat1
        , c_ssscat2.code                                                                            as s_sscat2
        , false                                                                                     as site
        , 'VN AuRA'                                                                                 as source
        , st_setsrid(st_makepoint("COORD_LAT_L93", "COORD_LON_L93"), 2154) :: geometry(POINT, 2154) as geom
        , "PASSAGE"                                                                                 as num_passage
      from stoc_vn_2018 o left join stoc_dico_code_points c_pluie
          on (c_pluie.type_code = 'pluie' and c_pluie.libelle = o."STOC_RAIN")
        left join stoc_dico_code_points c_vent
          on (c_vent.type_code = 'vent' and c_vent.libelle = o."STOC_WIND")
        left join stoc_dico_code_points c_nuage
          on (c_nuage.type_code = 'nuage' and c_nuage.libelle_vn = o."STOC_CLOUD")
        left join stoc_dico_code_points c_visi
          on (c_visi.type_code = 'visibilité' and c_visi.libelle = o."STOC_VISIBILITY")
        left join stoc_dico_code_points c_neige
          on (c_neige.type_code = 'neige' and c_neige.libelle_vn = o."STOC_SNOW")
        left join stoc_dico_code_points c_pmil1
          on (c_pmil1.type_code = 'habitat' and c_pmil1.libelle = o."HP1")
        left join stoc_dico_code_points c_ptyp1
          on (c_ptyp1.type_code = 'habitat' and c_pmil1.code = c_ptyp1.principal and c_ptyp1.libelle = o."HP2")
        left join stoc_dico_code_points c_pcat1
          on (c_pcat1.type_code = 'habitat' and c_pmil1.code = c_pcat1.principal and c_pcat1.libelle = o."HP3A")
        left join stoc_dico_code_points c_pcat2
          on (c_pcat2.type_code = 'habitat' and c_pmil1.code = c_pcat2.principal and c_pcat2.libelle = o."HP3B")
        left join stoc_dico_code_points c_psscat1
          on (c_psscat1.type_code = 'habitat' and c_pmil1.code = c_psscat1.principal and c_psscat1.libelle = o."HP4A")
        left join stoc_dico_code_points c_psscat2
          on (c_psscat2.type_code = 'habitat' and c_pmil1.code = c_psscat2.principal and c_psscat2.libelle = o."HP4B")
        left join stoc_dico_code_points c_smil1
          on (c_smil1.type_code = 'habitat' and c_smil1.libelle = o."HS1")
        left join stoc_dico_code_points c_styp1
          on (c_styp1.type_code = 'habitat' and c_pmil1.code = c_styp1.principal and c_styp1.libelle = o."HS2")
        left join stoc_dico_code_points c_scat1
          on (c_scat1.type_code = 'habitat' and c_pmil1.code = c_scat1.principal and c_scat1.libelle = o."HS3A")
        left join stoc_dico_code_points c_scat2
          on (c_scat2.type_code = 'habitat' and c_pmil1.code = c_scat2.principal and c_scat2.libelle = o."HS3B")
        left join stoc_dico_code_points c_ssscat1
          on (c_ssscat1.type_code = 'habitat' and c_pmil1.code = c_ssscat1.principal and c_ssscat1.libelle = o."HS4A")
        left join stoc_dico_code_points c_ssscat2
          on (c_ssscat2.type_code = 'habitat' and c_pmil1.code = c_ssscat2.principal and c_ssscat2.libelle = o."HS4B")
      where (split_part(split_part("PLACE",' ',1), '_', 1)::int, "DATE" :: date, "POINT_NUMBER") = datas.data
      group by
        "DATE" :: date
        , "TIME_START" :: time
        , (split_part(split_part("PLACE",' ',1), '_', 1))::int
        , "NAME"
        , "ID_REF"
        , "POINT_NUMBER"
        , c_nuage.code :: int
        , c_pluie.code :: int
        , c_vent.code :: int
        , c_visi.code :: int
        , c_pmil1.code
        , c_ptyp1.code
        , c_pcat1.code
        , c_pcat2.code
        , c_psscat1.code
        , c_psscat2.code
        , c_smil1.code
        , c_styp1.code
        , c_scat1.code
        , c_scat2.code
        , c_ssscat1.code
        , c_ssscat2.code
        , ("COORD_LAT_L93", "COORD_LON_L93")
        , "PASSAGE"
  ;

  end loop
  ;

  raise notice 'endLoop'
  ;

  return
  ;

end

$$
;

select distinct "STOC_VISIBILITY" from stoc_vn_2018;

/* Génération des données d'après les id de datas_to_keep */
  select distinct
    "DATE" :: date
    , "TIME_START" :: time
    , "NAME"
        , split_part(split_part("PLACE",' ',1), '_', 1)::int                                                              as place
        , "POINT_NUMBER"                                                                            as point
        , round(avg(distinct "ALTITUDE"))                                                                    as altitude
    , c_nuage.code :: int                                                                       as code_nuage
    , c_pluie.code :: int                                                                       as code_pluie
    , c_vent.code :: int                                                                        as code_wind
    , c_visi.code :: int                                                                        as code_visi
    , c_pmil1.code                                                                              as p_milieu
    , c_ptyp1.code                                                                              as p_type
    , c_pcat1.code                                                                              as p_cat1
    , c_pcat2.code                                                                              as p_cat2
    , c_psscat1.code                                                                            as p_sscat1
    , c_psscat2.code                                                                            as p_sscat2
    , c_smil1.code                                                                              as s_milieu
    , c_styp1.code                                                                              as s_type
    , c_scat1.code                                                                              as s_cat1
    , c_scat2.code                                                                              as s_cat2
    , c_ssscat1.code                                                                            as s_sscat1
    , c_ssscat2.code                                                                            as s_sscat2
    , false                                                                                     as site
    , 'VN AuRA'                                                                                 as source
    , st_setsrid(st_makepoint("COORD_LAT_L93", "COORD_LON_L93"), 2154) :: geometry(POINT, 2154) as geom
    , "PASSAGE"                                                                                 as num_passage
    , array_agg(distinct "ID_SIGHTING")                                                         as id_source
  from stoc_vn_2018 o left join stoc_dico_code_points c_pluie
      on (c_pluie.type_code = 'pluie' and c_pluie.libelle = o."STOC_RAIN")
    left join stoc_dico_code_points c_vent
      on (c_vent.type_code = 'vent' and c_vent.libelle = o."STOC_WIND")
    left join stoc_dico_code_points c_nuage
      on (c_nuage.type_code = 'nuage' and c_nuage.libelle_vn = o."STOC_CLOUD")
    left join stoc_dico_code_points c_visi
      on (c_visi.type_code = 'visibilité' and c_visi.libelle = o."STOC_VISIBILITY")
    left join stoc_dico_code_points c_neige
      on (c_neige.type_code = 'neige' and c_neige.libelle_vn = o."STOC_SNOW")
    left join stoc_dico_code_points c_pmil1
      on (c_pmil1.type_code = 'habitat' and c_pmil1.libelle = o."HP1")
    left join stoc_dico_code_points c_ptyp1
      on (c_ptyp1.type_code = 'habitat' and c_pmil1.code = c_ptyp1.principal and c_ptyp1.libelle = o."HP2")
    left join stoc_dico_code_points c_pcat1
      on (c_pcat1.type_code = 'habitat' and c_pmil1.code = c_pcat1.principal and c_pcat1.libelle = o."HP3A")
    left join stoc_dico_code_points c_pcat2
      on (c_pcat2.type_code = 'habitat' and c_pmil1.code = c_pcat2.principal and c_pcat2.libelle = o."HP3B")
    left join stoc_dico_code_points c_psscat1
      on (c_psscat1.type_code = 'habitat' and c_pmil1.code = c_psscat1.principal and c_psscat1.libelle = o."HP4A")
    left join stoc_dico_code_points c_psscat2
      on (c_psscat2.type_code = 'habitat' and c_pmil1.code = c_psscat2.principal and c_psscat2.libelle = o."HP4B")
    left join stoc_dico_code_points c_smil1
      on (c_smil1.type_code = 'habitat' and c_smil1.libelle = o."HS1")
    left join stoc_dico_code_points c_styp1
      on (c_styp1.type_code = 'habitat' and c_pmil1.code = c_styp1.principal and c_styp1.libelle = o."HS2")
    left join stoc_dico_code_points c_scat1
      on (c_scat1.type_code = 'habitat' and c_pmil1.code = c_scat1.principal and c_scat1.libelle = o."HS3A")
    left join stoc_dico_code_points c_scat2
      on (c_scat2.type_code = 'habitat' and c_pmil1.code = c_scat2.principal and c_scat2.libelle = o."HS3B")
    left join stoc_dico_code_points c_ssscat1
      on (c_ssscat1.type_code = 'habitat' and c_pmil1.code = c_ssscat1.principal and c_ssscat1.libelle = o."HS4A")
    left join stoc_dico_code_points c_ssscat2
      on (c_ssscat2.type_code = 'habitat' and c_pmil1.code = c_ssscat2.principal and c_ssscat2.libelle = o."HS4B")
  where (split_part(split_part("PLACE",' ',1), '_', 1)::int, "DATE" :: date, "POINT_NUMBER") = (30640, '2018-05-30', 1)
  group by
    "DATE" :: date
    , "TIME_START" :: time
    , "NAME"
    , split_part(split_part("PLACE",' ',1), '_', 1)
    , "ALTITUDE"
    , "POINT_NUMBER"
    , c_nuage.code :: int
    , c_pluie.code :: int
    , c_vent.code :: int
    , c_visi.code :: int
    , c_pmil1.code
    , c_ptyp1.code
    , c_pcat1.code
    , c_pcat2.code
    , c_psscat1.code
    , c_psscat2.code
    , c_smil1.code
    , c_styp1.code
    , c_scat1.code
    , c_scat2.code
    , c_ssscat1.code
    , c_ssscat2.code
    , false
    , ("COORD_LAT_L93", "COORD_LON_L93")
    , "PASSAGE"
;

select "HP4A"
from stoc_vn_2018
where "ID_SIGHTING" = 893094
;

delete from data.stoc_releves
where (carre_numnat, date, point_num) = (6, '2018-05-29', 10)
;

select *
from data.stoc_dico_code_points
where libelle like 'Pâturé'
;

insert into data.stoc_observations (
  carre_numnat,
  date,
  time,
  id_releve,
  point,
  codesp_euring,
  nombre,
  source,
  passage,
  distance_v2
)
  with rel as (
    /* génération d'id unique de relevé pour association des données mnhn (concatenation du carre, date, point) à la table stoc_releve */
      select
        id
        , concat(carre_numnat :: int, date :: date, point_num :: int) as concat
      from data.stoc_releves
      where source_bdd like 'MNHN' ),
      datas as (
      /* Table des obs avec la génération d'une concaténation comparable avec la précédente */
        select
          distinct
            obs.carre_num :: int                                     as carre
          , obs.date :: date
          , nullif(obs.heure, 'NA') :: time                          as heure
          , point_ok :: int                                          as point
          , obs.espece                                               as codesp_euring
          , sum(obs.nombre)                                          as nombre
          , obs.passage_v2
          , obs.distance_v2                                          as distance
          , concat(obs.carre_num, obs.date :: date, point_ok :: int) as concat
        from stoc_vn_2018 as obs
        group by
          obs.carre_num
          , obs.date
          , obs.heure
          , obs.point_ok
          , obs.espece
          , obs.passage_v2
          , obs.distance_v2
          , concat(obs.carre_num, obs.date :: date, point_ok :: int)
    )
  /* Récuparétion des id de relevés d'après l'association des deux précédentes requêtes*/
  select
    distinct
    obs.carre
    , obs.date
    , obs.heure
    , rel.id    as id_releve
    , obs.point
    , obs.codesp_euring
    , obs.nombre
    , 'VN AuRA' as source
    , obs.passage_v2
    , obs.distance
  from datas as obs, rel
  where obs.concat = rel.concat
;
