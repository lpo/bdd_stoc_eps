/*********************************************************************
 * Créatiop d''une bdd de centralisation des données STOC en AuRA   *
 *********************************************************************/

/*
Nettoyage de la table des mailles nationales (mailles en multipolygon...)
*/


--
-- alter table referentiel.stoc_grille_nationale
--   set schema lpo07_fcl
-- ;
--
-- drop table if exists referentiel.stoc_grille_nationale;

create table data.stoc_grille_aura as
  select distinct
    m.id
    , m.area
    , m.perimeter
    , m.carrenat
    , m.numnat
    , m.x_coord
    , m.y_coord
    , m.geom
  from referentiel.stoc_grille_nationale m,
    administratif.territoire_departement d
  where st_intersects(m.geom, d.geom)
;


alter table data.stoc_grille_aura
  add primary key (id)
;

create index on data.stoc_grille_aura
using gist (geom)
;

alter table data.stoc_grille_aura
  add constraint stoc_grille_aura_numnat unique (numnat)
;

-- drop table if exists lpo07_fcl.stoc_grille_nationale;

/* Table des points */
-- drop table if exists data.stoc_releves cascade;
create table data.stoc_releves
(
  id           serial primary key,
  date         date,
  heure        time,
  observateur  text,
  carre_numnat integer,
  point_num    integer,
  altitude     integer,
  nuage        integer,
  pluie        integer,
  vent         integer,
  visibilite   integer,
  p_milieu     varchar(10),
  p_type       varchar(10),
  p_cat1       varchar(10),
  p_cat2       varchar(10),
  p_ss_cat1    varchar(10),
  p_ss_cat2    varchar(10),
  s_milieu     varchar(10),
  s_type       varchar(10),
  s_cat1       varchar(10),
  s_cat2       varchar(10),
  s_ss_cat1    varchar(10),
  s_ss_cat2    varchar(10),
  site         boolean,
  source_bdd   varchar(50),
  geom         geometry(point, 2154),
  passage_mnhn varchar(10),
  id_source    text []
;


/* Index sur les colonnes carre_numnat, date et point_num */
create unique index on data.stoc_releves (carre_numnat, date, point_num)
;

create index on data.stoc_releves
using gist (geom)
;


/* Table des correspondances des distances */

create table data.stoc_dico_code_distances as
  select
    row_number()
    over () as id
    , *
  from lpo07_fcl.stoc_codes_distance
  order by code asc
;

alter table data.stoc_dico_code_distances
  add primary key (id)
;


create table data.stoc_dico_code_points as
  select
    row_number()
    over () as id
    , *
  from lpo07_fcl.stoc_dico_code_points
  order by type_code, colonne, code
;

alter table data.stoc_dico_code_points_old
  add primary key (id)
;

/* Table des observations */
-- drop table if exists data.stoc_observations cascade
-- ;

create table data.stoc_observations
(
  id            serial not null primary key,
  carre_numnat  integer,
  date          date,
  time          time,
  id_releve     integer references data.stoc_releves (id) on delete cascade,
  point         integer,
  codesp_euring varchar(10),
  nombre        integer,
  source        varchar(20),
  passage       varchar(20),
  distance_v2   varchar(50),
  id_source     text []
)
;

create unique index on data.stoc_observations (carre_numnat, date, id_releve, point, codesp_euring, distance_v2)
;

-- alter table data.stoc_observations
--   drop constraint stoc_observations_id_releve_fkey,
--   add constraint stoc_observations_id_releve_fkey
-- foreign key (id_releve)
-- references data.stoc_releves (id)
-- on delete cascade
-- ;

alter table data.stoc_observations
  alter column id_releve add constraint on delete cascade
;
